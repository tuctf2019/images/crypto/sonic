# Sonic -- Crypto -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/crypto/sonic)

## Chal Info

Desc: `Gotta go fast`

Flag: `TUCTF{W04H_DUD3_S0_F4ST_S0N1C_4PPR0V3S}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/sonic)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/sonic:tuctf2019
```
