#!/usr/bin/env python3

from select import select

import sys, random, requests, string

INTRO = """
    ___------__
 |\\__-- /\\       _-
 |/    __      -
 //\\  /  \\    /__
 |  o|  0|__     --_
 \\____-- __ \\   ___-
 (@@    __/  / /_
    -_____---   --_
    //  \\ \\\\   ___-
    //|\\__/  \\  \\
    \\_-\\_____/  \\-\\
        // \\\\--\\| 
    ____//  ||_
  /_____\\ /___\\

  Gotta go fast!

  """

def caesar(s, key):
    charset = string.ascii_uppercase
    l = len(charset)
    k = key % l
    out = ''.join([charset[(charset.index(c) + k) % l] if c in charset else c for c in s])
    return out

def timeoutfunc():
    sys.stdout.write('Too slow!\n')
    sys.stdout.flush()
    sys.exit(0)

if __name__ == '__main__':
    flag = "TUCTF{W04H_DUD3_S0_F4ST_S0N1C_4PPR0V3S}"

    #word_site = "http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"

    #response = requests.get(word_site)
    #WORDS = response.content.splitlines()
    with open('wordlist.txt', 'r') as f:
        WORDS = [line.rstrip('\n').encode('utf-8') for line in f.readlines()]

    key = random.randint(1, 25)

    sys.stdout.write(INTRO)
    sys.stdout.flush()

    word = random.choice(WORDS).decode('utf-8').upper()

    crypt = caesar(word, key)

    prompt = "\nHey, decode this: " + crypt + "\n"

    attempts = 0

    while attempts < 26:
        try:
            sys.stdout.write(prompt)
            sys.stdout.flush()

            rlist, _, _ = select([sys.stdin], [], [], 3)
            if rlist:
                data = sys.stdin.readline()
            else:
                timeoutfunc()

            data = data.rstrip('\n').upper()

            if data == word:
                sys.stdout.write("\nYou got it!\n\nHere's your prize:\n{}\n".format(flag))
                sys.stdout.flush()
                sys.exit(0)
            attempts += 1
        except KeyboardInterrupt:
            sys.stdout.write('\n')
            sys.stdout.flush()
            sys.exit(0)
