
FROM python:3

WORKDIR /usr/src/app

RUN apt -y update
RUN apt -y install socat
RUN pip install requests

COPY ./src .

CMD socat TCP-LISTEN:8888,fork,reuseaddr EXEC:"/usr/src/app/chal.py"
